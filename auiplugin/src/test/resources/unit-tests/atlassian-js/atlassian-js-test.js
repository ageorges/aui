Qunit.requireCss('unit-tests/atlassian-js/atlassian-js-test.css');
Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');


module("Unit Tests for atlassian.js");

// AJS.id and AJS._addID tests are affected by order.
// Qunit reruns failed tests out of order unless you set this to false:
QUnit.config.reorder = false;
// ...now on with the show.

test("AJS.version", function() {
    ok( typeof AJS.version === "string", " AJS.version should return a string (in REFAPP it will be the generic project.version string)");
});

test("AJS.alphanum", function() {
    function assertAlphaNum(a, b, expected) {
        var actual = AJS.alphanum(a, b);
        equal(actual, expected, "alphanum test\n" + a + "\n" + b);

        // try in reverse
        actual = AJS.alphanum(b, a);
        equal(actual, expected * -1, "alphanum (reverse) test\n" + b + "\n" + a)
    }

    assertAlphaNum("a", "a", 0);
    assertAlphaNum("a", "b", -1);
    assertAlphaNum("b", "a", 1);

    assertAlphaNum("a0", "a1", -1);
    assertAlphaNum("a10", "a1", 1);
    assertAlphaNum("a2", "a1", 1);
    assertAlphaNum("a2", "a10", -1);
});

test("AJS.escapeHtml", function() {

    equal(AJS.escapeHtml("a \" doublequote"), "a &quot; doublequote");
    equal(AJS.escapeHtml("a ' singlequote"), "a &#39; singlequote");
    equal(AJS.escapeHtml("a < lessthan"), "a &lt; lessthan");
    equal(AJS.escapeHtml("a > greaterthan"), "a &gt; greaterthan");
    equal(AJS.escapeHtml("a & ampersand"), "a &amp; ampersand");
    equal(AJS.escapeHtml("a ` accent grave"), "a &#96; accent grave");

    equal(AJS.escapeHtml("foo"), "foo");

    equal(AJS.escapeHtml("<foo>"), "&lt;foo&gt;");
    equal(AJS.escapeHtml("as<foo>as"), "as&lt;foo&gt;as");

    equal(AJS.escapeHtml("some <input class=\"foo\" value='bar&wombat'> thing"), "some &lt;input class=&quot;foo&quot; value=&#39;bar&amp;wombat&#39;&gt; thing");
});

test("AJS.isClipped", function() {
    equal(AJS.isClipped( AJS.$("#shouldBeClipped")), true, "Should be clipped");
    equal(AJS.isClipped( AJS.$("#shouldNotBeClipped")), false, "Should not be clipped");
});

test("AJS.populateParameters with list inputs", function() {
    AJS.$('<fieldset class="parameters"><input title="test1" class="list" value="value1"><input title="test1" class="list" value="value2"></fieldset>').appendTo("#qunit-fixture");
    AJS.populateParameters();
    equal(AJS.params.test1.length, 2);
    equal(AJS.params.test1[0], "value1");
    equal(AJS.params.test1[1], "value2");
});

test("AJS.populateParameters with no parameter", function() {
    AJS.$('<fieldset class="parameters"><input id="test1" value="value1"></fieldset>').appendTo("#qunit-fixture");
    AJS.populateParameters();
    equal(AJS.params.test1, "value1");

    // Clean up for future tests
    AJS.params = {};
});

test("AJS.populateParameters with parameter", function() {
    AJS.$('<fieldset class="parameters"><input id="test1" value="value1"></fieldset>').appendTo("#qunit-fixture");
    var toPopulate = {};
    AJS.populateParameters(toPopulate);
    equal(toPopulate.test1, "value1");
    equal(AJS.params.test1, undefined);
});

module("AJS.toInit");

test("Add multiple functions", function() {
    var func1 = sinon.spy();
    var func2 = sinon.spy();

    AJS.toInit(func1);
    AJS.toInit(func2);

    ok(func1.calledOnce, "should have been called");
    ok(func2.calledOnce, "should have been called");
});

test("Throw error", function() {

    var func1 = sinon.stub().throws("WTF");
    var func2 = sinon.spy();

    AJS.toInit(func1);
    AJS.toInit(func2);

    ok(func1.threw, "should have thrown an exception");
    ok(func2.calledOnce, "should have been called");
});

test("AJS.id default test", function() {
    QUnit.equal(AJS.id(), "aui-uid-0");
});

test("AJS.id prefix test", function() {
    QUnit.equal(AJS.id("foo"), "foo1");
});

test("AJS._addID default test", function() {
    var $el = AJS.$('<div></div>').appendTo("#qunit-fixture");
    AJS._addID($el);
    QUnit.equal($el.attr("id"), "aui-uid-2");
});

test("AJS._addID prefix test", function() {
    var $el = AJS.$('<div></div>').appendTo("#qunit-fixture");
    AJS._addID($el, "foo");
    QUnit.equal($el.attr("id"), "foo3");
});

test("AJS._addID multiple element test", function() {
    var $el = AJS.$('<div class="idmultiple"></div><div class="idmultiple"></div><div class="idmultiple"></div>').appendTo("#qunit-fixture");
    AJS._addID(AJS.$(".idmultiple"));
    QUnit.equal(AJS.$(".idmultiple:eq(0)").attr("id"),"aui-uid-4");
    QUnit.equal(AJS.$(".idmultiple:eq(1)").attr("id"),"aui-uid-5");
    QUnit.equal(AJS.$(".idmultiple:eq(2)").attr("id"),"aui-uid-6");
});

test("AJS.id string", function() {
    QUnit.equal(typeof AJS.id(), "string");
});

module("I18n Unit Tests", {
    teardown: function() {
        if(AJS.I18n.keys) {
            delete AJS.I18n.keys;
        }
    }
});

test("AJS.I18n.getText return key test", function(){
    equal(AJS.I18n.getText("test.key"), "test.key");
});

test("AJS.I18n.getText return value test", function(){
    AJS.I18n.keys = {
        "test.key": "This is a Value"
    }
    equal(AJS.I18n.getText("test.key"), "This is a Value");
});

test("AJS.I18n.getText return value test", function(){
    AJS.I18n.keys = {
        "test.key": "Formatting {0}, and {1}"
    }
    equal(AJS.I18n.getText("test.key", "hello", "world"), "Formatting hello, and world");
});